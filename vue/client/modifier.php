<div class="card-panel">
<?php
echo "<form method=\"";
if (Conf::getDebug()) {
    echo "get";
}
else {
    echo "post";
}
echo "\" action=\"index.php\">";
?>
<?php 
$prenom = "";
$nom = "";
$login = "";
$mail = "";

if(isset($nom1)){
    $nom = $_GET["nom"];
}
if(isset($login1)){
    $login = $_GET["login"];
}
if(isset($mail1)){
    $mail = $_GET["mail"];
}
if(isset($prenom1)){
    $prenom = $_GET["prenom"];
}

?>
  <fieldset>
    <legend> <?php echo $creer ?> </legend>
    <?php
    echo '<input type="hidden" value="';
    if ($actionModif == "modifier") {
        echo 'modifie';
        $prenom = htmlspecialchars($c->get("prenom"));
        $nom = htmlspecialchars($c->get("nom"));
        $login = htmlspecialchars($c->get("login"));
        $mail = htmlspecialchars($c->get("mail"));
    }
    if ($actionModif == "creer") {
        echo 'cree';
    }
    echo '" name="action"/>';
    ?>
    <?php
    echo '<input type="text" value="'.htmlspecialchars(static::$object).'" placeholder="Ex : Produit" name="controleur" id="control_id" required hidden/>';
    ?>
    <p>
      <label for="login_id">Login</label> :
      <?php
      echo '<input type="text" value="'. htmlspecialchars($login) .'" placeholder="Ex : Xx_Dark_Sasuke_69420_xX" name="login" id="login_id" required/>';
      ?>
    </p>
    <p>
      <label for="prenom_id">Prenom</label> :
      <?php
      echo '<input type="text" value="'.htmlspecialchars($prenom).'" placeholder="Ex : Dupon" name="prenom" id="prenom_id" required/>';
      ?>
    </p>
    <p>
      <label for="nom_id">Nom</label> :
      <?php
      echo '<input type="text" value="'.htmlspecialchars($nom).'" placeholder="Ex : Jean" name="nom" id="nom_id" required/>';
      ?>
    </p>
    <p>
      <label for="mail_id">Mail</label> :
      <?php
      echo '<input type="email" value="'.htmlspecialchars($mail).'" '
          .'placeholder="Ex : exemple@exemple.com" name="mail" id="mail_id" required/>';
      ?>
    </p>
    <p>
      <label for="passCode_id">Mot de passe</label> :
      <?php
      echo '<input type="password" placeholder="Ex : 1fa3Eh54$ù" name="passCode" id="passCode_id" required/>';
      ?>
    </p>
    <p>
      <label for="passCode2_id">Confirmez Mot de passe</label> :
      <?php
      echo '<input type="password" placeholder="Ex : copiez le truc au dessu" name="passCode2" id="passCode2_id" required/>';
      ?>
    </p>
    <p>
        <button class="btn waves-effect waves-light" type="submit" value="envoyer">Envoyer</button>
    </p>
  </fieldset> 
</form>
</div>