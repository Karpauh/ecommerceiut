<?php
require_once File::build_path(array('modele', 'ModeleProduit.php'));


class ControleurProduit {
    
    protected static $object = "produit";
    
    public static function lireTout() {
        $tab_p = ModeleProduit::selectionnerTout();     //appel au modèle pour gerer la BD
        $vue = 'liste';
        $pagetitle = 'Liste des produits';
        require File::build_path(array('vue', 'vue.php'));  //"redirige" vers la vue
    }
    
    public static function lire() {
        $idProduit = myGet("idProduit");
        $p = ModeleProduit::selectionner($idProduit);
        if ($p == false) {
            $vue = 'error';
            $pagetitle = 'Erreur produit innexistant';
            require File::build_path(array('vue', 'vue.php'));
        }
        else {
            $imagesPath = ModeleProduit::selectionnerToutImageDuProduit($idProduit);
            $tab_c = ModeleProduit::selectionnerToutCategoriesDuProduit($idProduit);
            $vue = 'detail';
            $pagetitle = 'Detail de Produit';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function modifier() {
        if (Session::est_admin()) {
            $idProduit = myGet("idProduit");
            $p = ModeleProduit::selectionner($idProduit);
            $tab_c = ModeleCategorie::selectionnerTout();
            $actionModif = "modifier";
            $vue = 'modifier';
            $pagetitle = 'Mise a jour du produit';
            require File::build_path(array('vue', 'vue.php'));
        }
        else {
            $tab_p = ModeleProduit::selectionnerTout();
            $vue = 'liste';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function modifie() {
        if (Session::est_admin()) {
            $data = array(
                'idProduit' => myGet('idProduit'),
                'libele' => myGet('libele'),
                'nombre' => myGet('nombre'),
                'imagePrincipale' => myGet('imagePrincipale'),
                'prix' => myGet('prix'),
            );
            if (is_numeric(myGet('nombre')) && is_numeric(myGet('prix'))) {
                if ($data['prix'] < 0) {
                    $data['prix'] = 0;
                } 
                $isGood = ModeleProduit::modifier($data);
            }
            else {
                $idProduit = $data['idProduit'];
                $vue = 'error';
                $pagetitle = 'Erreur nombre ou prix non numeric';
                require File::build_path(array('vue', 'vue.php'));
                return;
            }
            ModeleCategorie::supprimerToutcateid(myGet('idProduit'));
            foreach (myGet('idCategorie') as $value) {
                $data0 = array(
                    'idProduit' => myGet('idProduit'),
                    'idCategorie' => $value,
                );

                if (!is_numeric($data0['idProduit']) || !is_numeric($data0['idCategorie'])) {
                    $vue = 'error';
                    $pagetitle = 'Erreur champ categorie non valide';
                    require File::build_path(array('vue', 'vue.php'));
                    return;
                }
                $isGood0 = ModeleCategorie::associerProduitCategorie($data0);
            }


            if ($isGood == false || $isGood0 == false) {
                $idProduit = $data['idProduit'];
                $vue = 'error';
                $pagetitle = 'Erreur de mise a jour de Produit';
                require File::build_path(array('vue', 'vue.php'));
            }
            else {
                $tab_p = ModeleProduit::selectionnerTout();
                $vue = 'modifie';
                $pagetitle = 'Produit modifie';
                require File::build_path(array('vue', 'vue.php'));
            }
        }
        else {
            $tab_p = ModeleProduit::selectionnerTout();
            $vue = 'liste';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function creer() {

        if (Session::est_admin()) {
            $p = new ModeleProduit();
            $tab_c = ModeleCategorie::selectionnerTout();
            $actionModif = "creer";
            $vue = 'modifier';
            $pagetitle = 'Creation de Produit';
            require File::build_path(array('vue', 'vue.php'));
        }
        else {
            $tab_p = ModeleProduit::selectionnerTout();
            $vue = 'liste';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }

    public static function cree() {
        if (Session::est_admin()) {
            $data = array(
                'libele' => myGet('libele'),
                'nombre' => myGet('nombre'),
                'imagePrincipale' => myGet('imagePrincipale'),
                'prix' => myGet('prix'),
            );
            if ($data['prix'] < 0) {
                $data['prix'] = 0;
            }
            if (empty(myGet('idProduit'))) {
                $data['idProduit'] = ModeleProduit::selectMaxId()+1;
            }
            else {
                $data['idProduit'] = myGet('idProduit');
            }
            if (!is_numeric($data['idProduit']) || empty($data['libele']) || !is_numeric($data['nombre']) || !is_numeric($data['imagePrincipale']) || !is_numeric($data['prix'])) {
                $vue = 'error';
                $pagetitle = 'Erreur champ produit non valide';
                require File::build_path(array('vue', 'vue.php'));
                return;
            }
            
            $isGood = ModeleProduit::creer($data);

            foreach (myGet('idCategorie') as $value) {
                $data0 = array(
                    'idProduit' => $data['idProduit'],
                    'idCategorie' => $value,
                );

                if (!is_numeric($data0['idProduit']) || !is_numeric($data0['idCategorie'])) {
                    $vue = 'error';
                    $pagetitle = 'Erreur champ categorie non valide';
                    require File::build_path(array('vue', 'vue.php'));
                    return;
                }
                $isGood0 = ModeleCategorie::associerProduitCategorie($data0);
            }
            if ($isGood == false || $isGood0 == false) {
                $vue = 'error';
                $pagetitle = 'Erreur de creation de Produit';
                require File::build_path(array('vue', 'vue.php'));
            }
            else {
                $tab_p = ModeleProduit::selectionnerTout();
                $vue = 'cree';
                $pagetitle = 'Produit cree';
                require File::build_path(array('vue', 'vue.php'));
            }
        }
        else {
            $tab_p = ModeleProduit::selectionnerTout();
            $vue = 'liste';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function supprimmer() {
        if (Session::est_admin()) {
            $idProduit = myGet('idProduit');
            $isGood = ModeleProduit::supprimmer($idProduit);
            if ($isGood == false) {
                $vue = 'error';
                $pagetitle = 'Erreur de suppression de Produit';
                require File::build_path(array('vue', 'vue.php'));
            }
            else {
                $tab_p = ModeleProduit::selectionnerTout();
                $vue = 'supprimme';
                $pagetitle = 'Produit supprime';
                require File::build_path(array('vue', 'vue.php'));
            }
        }
        else {
            $tab_p = ModeleProduit::selectionnerTout();
            $vue = 'liste';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function ajouterPanier() {
        $idProduit = myGet('idProduit');
        $quantite = myGet('quantite');
        if (!is_numeric($quantite)) {
            $vue = 'error';
            $pagetitle = 'Erreur quantité non numérique';
            require File::build_path(array('vue', 'vue.php'));
            return false;
        }
        $p = ModeleProduit::selectionner($idProduit);
        if ($p == false) {
            $vue = 'error';
            $pagetitle = 'Erreur produit innexistant';
            require File::build_path(array('vue', 'vue.php'));
            return false;
        }
        if (is_null(myCookie('panier'))) {
            setcookie("panier", serialize(array()), time() + 600);
        }
        $totalPanier = unserialize(myCookie('panier'));
        if (!isset($totalPanier[$idProduit])) {
            $totalPanier[$idProduit] = 0;
        }
        $totalPanier[$idProduit] += $quantite;
        if ($totalPanier[$idProduit]>$p->get('nombre')) {
            $imagesPath = ModeleProduit::selectionnerToutImageDuProduit($idProduit);
            $tab_c = ModeleProduit::selectionnerToutCategoriesDuProduit($idProduit);
            $vue = 'detail';
            $pagetitle = 'Quantite trop elevee';
            require File::build_path(array('vue', 'vue.php'));
            return false;
        }
        $tab_p = array();
        foreach ($totalPanier as $key => $value) {
            $p = ModeleProduit::selectionner($key);
            $tab_p[serialize($p)] = $value;
        }
        setcookie("panier", serialize($totalPanier), time() + 600);
        $_SESSION['panier'] = myCookie('panier');
        $_SESSION['prixPanier'] = ControleurProduit::calculerPrixPanier($tab_p);
        $tab_p = ModeleProduit::selectionnerTout();
        $vue = 'ajoute';
        $pagetitle = 'Produit ajoute';
        require File::build_path(array('vue', 'vue.php'));
    }
    
    public static function calculerPrixPanier($tab_p) {
        $prix = 0;
        foreach ($tab_p as $p => $nombre) {
            $p = unserialize($p);
            $prix += $p->get('prix') * $nombre;
        }
        return $prix;
    }
    
    public static function panier() {
        if (!is_null(myCookie('panier'))) {
            $panier = unserialize(myCookie('panier'));
        }
        else {
            $panier = array();
            setcookie("panier", serialize(array()), time() + 600);
        }
        $tab_p = array();
        foreach ($panier as $key => $value) {
            $p = ModeleProduit::selectionner($key);
            $tab_p[serialize($p)] = $value;
        }
        $_SESSION['panier'] = myCookie('panier');
        $_SESSION['prixPanier'] = ControleurProduit::calculerPrixPanier($tab_p);
        $vue = 'panier';
        $pagetitle = 'Liste du panier';
        require File::build_path(array('vue', 'vue.php'));
    }
    
    public static function supprimerPanier() {
        $idProduit = myGet('idProduit');
        $quantite = myGet('quantite');
        if (!is_numeric($quantite)) {
            $vue = 'error';
            $pagetitle = 'Erreur quantité non numérique';
            require File::build_path(array('vue', 'vue.php'));
            return false;
        }
        if (!is_null(myCookie('panier'))) {
            $panier = unserialize(myCookie('panier'));
            foreach ($panier as $idProduitPanier => $nombre) {
                if ($idProduitPanier == $idProduit) {
                    $panier[$idProduitPanier]-=$quantite;
                    if ($panier[$idProduitPanier] <= 0) {
                        unset($panier[$idProduitPanier]);
                    }
                    break;
                }
            }
            setcookie("panier", serialize($panier), time() + 600);
        }
        else {
            $panier = array();
        }
        $tab_p = array();
        foreach ($panier as $key => $value) {
            $p = ModeleProduit::selectionner($key);
            $tab_p[serialize($p)] = $value;
        }
        $_SESSION['panier'] = myCookie('panier');
        $_SESSION['prixPanier'] = ControleurProduit::calculerPrixPanier($tab_p);
        $vue = 'supprimePanier';
        $pagetitle = 'Produit Supprime';
        require File::build_path(array('vue', 'vue.php'));
    }

    
    public static function chercher(){

        $data = array(
            'libele' => myGet("libele"),
            'prix' => myGet("prix"),
        );

        if (empty($data['libele']) && empty($data['prix'])) {
            $vue = "formulaireRecherche";
            $pagetitle = "Erreur : champs vides";
            require File::build_path(array('vue', 'vue.php'));
            return;
        } elseif(!empty($data['prix']) && !is_numeric($data['prix'])) {
            $vue = "formulaireRecherche";
            $pagetitle = "Erreur : prix non numerique";
            require File::build_path(array('vue', 'vue.php'));
            return;
        }

        
        $tab_produit = ModeleProduit::chercher($data);
        $vue = "rechercher";
        $pagetitle = "Résultat de la recherche";
        require File::build_path(array('vue', 'vue.php'));

    }

    public static function chercherUnProduit(){
        $vue = "formulaireRecherche";
        $pagetitle = "Chercher un produit";
        require File::build_path(array('vue', 'vue.php'));
    }

    public static function passerCommande(){
        if (isset($_SESSION['login'])) {
            $login = $_SESSION['login'];
            $c = ModeleClient::selectionner($login);

            $panier = unserialize($_SESSION['panier']);
            if (empty($panier)) {
                $vue = 'error';
                $pagetitle = 'Erreur, panier vide';
                require File::build_path(array('vue', 'vue.php'));
                return;
            }
            $tab_p = array();
            foreach ($panier as $key => $nombre) {
                $p = ModeleProduit::selectionner($key);
                $tab_p[serialize($p)] = $nombre;
                $data = array(
                    'idProduit' => $p->get('idProduit'),
                    'nombre' => $p->get('nombre')-$nombre,
                 );
                ModeleProduit::modifier($data);
                $data = array(
                    'login' => $c->get('login'),
                    'idProduit' => $p->get('idProduit'),
                    'date' => date("Y-m-d H:i:s"),
                    'nombre' => $nombre,
                );
                ModeleAchat::creer($data);
            }

            $mailCLi = $c->get('mail');
            $mail = Mail::sendRecap($login, $tab_p);
            mail($mailCLi, "Recapitulatif commande", $mail);

            setcookie('panier', NULL, -1);
            unset($_SESSION['panier']);
            unset($_SESSION['prixPanier']);
            $vue = "commandePassee";
            $pagetitle = "Commande effectuée";
            require File::build_path(array('vue', 'vue.php'));
        }
        else {
            $vue = 'error';
            $pagetitle = 'Erreur, veuillez vous connecter';
            require File::build_path(array('vue', 'vue.php'));
        }
    }


    public static function laisserAvis(){
        $idProduit = myGet("idProduit");
        $p = ModeleProduit::selectionner($idProduit);

        $data = array('commentaire' => $_POST["commentaire"],
                    'idProduit' =>myGet("idProduit"),
                    'loginUser' => $_SESSION["login"]);
                    
        ModeleProduit::laisserAvis($data);
        $tab_avis = ModeleProduit::recupAvis($data);
        
        $tab_c = ModeleProduit::selectionnerToutCategoriesDuProduit($idProduit);
        $imagesPath = ModeleProduit::selectionnerToutImageDuProduit($idProduit);
        $vue = 'detail';
        $pagetitle = 'Détail du produit';
        require File::build_path(array('vue', 'vue.php'));
    }

}
