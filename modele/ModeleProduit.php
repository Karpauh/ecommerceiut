<?php
require_once File::build_path(array('modele', 'Modele.php'));

class ModeleProduit extends Modele {
    
    protected static $object = "produit";
    protected static $primary = 'idProduit';
        
    private $idProduit;
    private $libele;
    private $nombre;
    private $imagePrincipale;
    private $prix;


    public function get($nom_attribut) {
        return $this->$nom_attribut;
    }
    
    public function set($nom_attribut, $valeur) {
        $this->$nom_attribut = $valeur;
    }
    
    public function __construct($data = NULL) {
        foreach ((array)$data as $cle => $value) {
            $this->set($cle, $value);
        }
    }
    
    public static function selectionnerToutImageDuProduit($idProduit) {
        try {
            $rep = Modele::$pdo->prepare("SELECT I.idImage"
                    . " FROM image I"
                    . " JOIN produit P on P.idProduit = I.idProduit"
                    . " WHERE P.idProduit = :ip");
            
            $values = array(
                "ip" => $idProduit,
            );
            
            $rep->execute($values);

            $rep->setFetchMode(PDO::FETCH_COLUMN, 0);
            return (array)$rep->fetchAll();
        }
        catch(PDOException $e) {
            if (Conf::getDebug()) {
                echo $e->getMessage(); // affiche un message d'erreur
            } else {
                echo 'Une erreur est survenue <a href="index.php?action=lireTout"> retour a la page d\'accueil </a>';
            }
            die();
        }
    }

    public static function chercher($data){
        $req_sql = "SELECT * FROM produit WHERE ";
        $yauntrucavant = false;
        $valeurs = array();

        if($data["libele"] != ""){
            $libele = $data["libele"];
            $req_sql = $req_sql . "produit.libele LIKE :libele";
            $yauntrucavant = true;
            $valeurs["libele"] = "%".$data["libele"]."%";
            
        }
        
        
        if($data["prix"] != 0){
            if($yauntrucavant){
                $req_sql = $req_sql . ' AND ';
            }
            $prix = intval($data["prix"]);
            $req_sql =  $req_sql . "produit.prix LIKE :prix";
            $valeurs["prix"] = "%".$data["prix"]."%";
            $yauntrucavant = true;
            
        }

        /*if(!(is_null($data["categorie"]))){
            if($yauntrucavant){
                $req_sql = $req_sql . ' AND ';
            }
            $categorie = $data["categorie"];
            $req_sql = $req_sql . 'produit.categorie = :categorie';
            $valeurs["categorie"] = $data["categorie"];
        }*/

        try{
            $rep = Modele::$pdo->prepare($req_sql);

            $rep->execute($valeurs);
            
            $rep->setFetchMode (PDO::FETCH_CLASS, 'ModeleProduit');

            return $rep->fetchAll();
        }
        catch(PDOException $e) {
            if (Conf::getDebug()) {
                echo $e->getMessage(); // affiche un message d'erreur
            } else {
                echo 'Une erreur est survenue <a href="index.php?action=lireTout"> retour a la page d\'accueil </a>';
            }
            die();
        }

    }
    
    public static function selectionnerToutCategoriesDuProduit($idProduit) {
        try {
            $rep = Modele::$pdo->prepare("SELECT C.idCategorie, C.nomCategorie, C.imgCategorie"
                    . " FROM categorie C"
                    . " JOIN categorieProduit CP on CP.idCategorie = C.idCategorie"
                    . " WHERE CP.idProduit = :ip");
            
            $values = array(
                "ip" => $idProduit,
            );
            
            $rep->execute($values);

            $rep->setFetchMode(PDO::FETCH_CLASS, "ModeleCategorie");
            return (array)$rep->fetchAll();
        }
        catch(PDOException $e) {
            if (Conf::getDebug()) {
                echo $e->getMessage(); // affiche un message d'erreur
            } else {
                echo 'Une erreur est survenue <a href="index.php?action=lireTout"> retour a la page d\'accueil </a>';
            }
            die();
        }
    }
    
    public static function selectMaxId() {
        try {
            $rep = Modele::$pdo->query("SELECT MAX(P.idProduit)"
                    . " FROM produit P");


            $rep->setFetchMode(PDO::FETCH_COLUMN, 0);
            return $rep->fetchAll()[0];
        }
        catch(PDOException $e) {
            if (Conf::getDebug()) {
                echo $e->getMessage(); // affiche un message d'erreur
            } else {
                echo 'Une erreur est survenue <a href="index.php?action=lireTout"> retour a la page d\'accueil </a>';
            }
            die();
        }
    }

    public static function recupAvis($data){
        $req_sql = "SELECT message FROM avis WHERE idProduit = :idProduit";

        $rep = Modele::$pdo->prepare($req_sql);
            
            $values = array(
                "idProduit" => $data["idProduit"] 
            );
            
            
            $rep->execute($values);
            $rep->setFetchMode(PDO::FETCH_COLUMN, 0);
            return (array)$rep->fetchAll();
        
    }
   
}