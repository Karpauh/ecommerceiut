<?php
echo '<p> Client de login ' . htmlspecialchars($c->get("login")) . '.</p>';
echo '<p> Client de prenom ' . htmlspecialchars($c->get("prenom")) . '.</p>';
echo '<p> Client de nom ' . htmlspecialchars($c->get("nom")) . '.</p>';
echo '<p> Client de mail ' . htmlspecialchars($c->get("mail")) . '.</p>';

if (Session::est_utilisateur($c->get('login'))) {
    echo '<p>'
            . '<a href="index.php?action=modifier&controleur=client&'
            . 'login='.rawurlencode($c->get("login")).'" class="btn waves-effect waves-light">'
                . 'Modifier'
            . '</a>';
    echo '<a href="index.php?action=supprimmer&controleur=client&'
            . 'login='.rawurlencode($c->get("login")).'" class="btn waves-effect waves-light">'
                . 'Supprimer'
            . '</a>'
        . '</p>'."\n";
}
if (Session::est_admin()&&$_SESSION['login']!=$c->get('login')) {
    if($estadmin){
        echo '<p>'
            . '<a href="index.php?action=changeAdmin&controleur=client&'
            . 'login='.rawurlencode($c->get("login")).'" class="btn waves-effect waves-light">'
            . 'Enlever les droits d\'administrateur'
            . '</a>'
            . '</p>';
    }
    else{
        echo '<p>'
            . '<a href="index.php?action=changeAdmin&controleur=client&'
            . 'login='.rawurlencode($c->get("login")).'" class="btn waves-effect waves-light">'
            . 'Donner les droits d\'administrateur'
            . '</a>'
            . '</p>';
    }
}
