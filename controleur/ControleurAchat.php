<?php
require_once File::build_path(array('modele', 'ModeleAchat.php'));

class ControleurAchat {
    protected static $object = "achat";
    
    public static function lireTout() {
        if (Session::est_admin()) {
            $tab_a = ModeleAchat::selectionnerTout();     //appel au modèle pour gerer la BD
            $vue = 'liste';
            $pagetitle = 'Liste des achats';
            require File::build_path(array('vue', 'vue.php'));  //"redirige" vers la vue
        }
        else {
            $vue = 'error';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function lire() {
        if (Session::est_admin()) {
            $idAchat = myGet("idAchat");
            $a = ModeleAchat::selectionner($idAchat);
            if ($a == false) {
                $vue = 'error';
                $pagetitle = 'Erreur de lecture';
                require File::build_path(array('vue', 'vue.php'));
            }
            else {
                $tab_a = ModeleAchat::selectionner($idAchat);
                $vue = 'detail';
                $pagetitle = 'Detail de Achat';
                require File::build_path(array('vue', 'vue.php'));
            }
        }
        else {
            $vue = 'error';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function modifier() {
        if (Session::est_admin()) {
            $idAchat = myGet("idAchat");
            $a = ModeleAchat::selectionner($idAchat);

            $actionModif = "modifier";
            $vue = 'modifier';
            $pagetitle = 'Mise a jour de l\'achat';
            require File::build_path(array('vue', 'vue.php'));
        }
        else {
            $vue = 'error';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function modifie() {
        if (Session::est_admin()) {
            $data = array(
                'idAchat' => myGet('idAchat'),
                'idProduit' => myGet('idProduit'),
                'date' => myGet('date'),
                'nombre' => myGet('nombre'),
                'idAchat' => myGet('idAchat'),
            );
            if (empty($data['date']) || !is_numeric($data['idProduit']) || empty($data['nombre']) || !is_numeric($data['idAchat']) || empty($data['login'])) {
                $vue = 'error';
                $pagetitle = 'Erreur champ invalide';
                require File::build_path(array('vue', 'vue.php'));
            }
            $isGood = ModeleAchat::modifier($data);

            if ($isGood == false) {
                $idAchat = $data['idAchat'];
                $vue = 'error';
                $pagetitle = 'Erreur de mise a jour de Achat';
                require File::build_path(array('vue', 'vue.php'));
            }
            else {
                $tab_a = ModeleAchat::selectionnerTout();
                $vue = 'modifie';
                $pagetitle = 'Achat modifie';
                require File::build_path(array('vue', 'vue.php'));
            }
        }
        else {
            $vue = 'error';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function creer() {
        if (Session::est_admin()) {
            $a = new ModeleAchat();
            $actionModif = "creer";
            $vue = 'modifier';
            $pagetitle = 'Creation de Achat';
            require File::build_path(array('vue', 'vue.php'));
        }
        else {
            $vue = 'error';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function cree() {
        if (Session::est_admin()) {
            $data = array(
                    'idProduit' => myGet('idProduit'),
                    'date' => date("Y-m-d H:i:s"),
                    'nombre' => myGet('nombre'),
                    'idAchat' => myGet('idAchat'),
                    'login' => myGet('login'),
                );
            if (!is_numeric($data['idProduit']) || empty($data['nombre']) || !is_numeric($data['idAchat']) || empty($data['login'])) {
                $vue = 'error';
                $pagetitle = 'Erreur champ invalide';
                require File::build_path(array('vue', 'vue.php'));
            }
            $isGood = ModeleAchat::creer($data);
            if ($isGood == false) {
                $vue = 'error';
                $pagetitle = 'Erreur de creation de Achat';
                require File::build_path(array('vue', 'vue.php'));
            }
            else {
                $vue = 'cree';
                $pagetitle = 'Achat enregistré';
                require File::build_path(array('vue', 'vue.php'));
            }
        }
        else {
            $vue = 'error';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function supprimmer() {
        if (Session::est_Admin()) {
            $idAchat = myGet('idAchat');
            $isGood = ModeleAchat::supprimmer($idAchat);
            if ($isGood == false) {
                $vue = 'error';
                $pagetitle = 'Erreur de suppression de Achat';
                require File::build_path(array('vue', 'vue.php'));
            }
            else {
                $tab_a = ModeleAchat::selectionnerTout();
                $vue = 'supprimme';
                $pagetitle = 'Achat supprime';
                require File::build_path(array('vue', 'vue.php'));
            }
        }
        else {
            $vue = 'error';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
}
