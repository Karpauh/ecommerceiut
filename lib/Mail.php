<?php
class mail
{
    public static function getMail($login, $nonce)
    {
        return "<p>Bonjour " . htmlspecialchars($login) . ",</p>
                <p>
                    Veillez confirmer votre adresse mail en cliquant sur le lien suivant :
                </p>
                <p><a href=\"http://webinfo.iutmontp.univ-montp2.fr/~clementa/eCommerce/index.php?controleur=client&action=valider&login=" . htmlspecialchars($login) . "&nonce=" . htmlspecialchars($nonce) . "\">Valider</a></p>";
    }

    public static function sendRecap($login, $produit)
    {
        $test = "<p>Bonjour " . htmlspecialchars($login) . ",</p>
                <p>
                   Merci de nous avoir fait confiance, votre commande à bien été prise en compte par nos équipes, voici le récapitulatif de votre commande :
                </p>";
        foreach ($produit as $p => $nombre) {
            $p = unserialize($p);
            $test = $test . '<p> x' . $nombre . ' : ' . $p->get('libele');
        }
        return $test;
    }
}
?>


