<?php
require_once File::build_path(array('modele', 'ModeleCategorie.php'));
require_once File::build_path(array('lib', 'Securite.php'));
require_once File::build_path(array('lib', 'Mail.php'));

class ControleurCategorie {
    protected static $object = "categorie";
    
    public static function lireTout() {
        $tab_c = ModeleCategorie::selectionnerTout();     //appel au modèle pour gerer la BD
        $vue = 'liste';
        $pagetitle = 'Liste des categories';
        require File::build_path(array('vue', 'vue.php'));  //"redirige" vers la vue
    }
    
    public static function lire() {
        $idCategorie = myGet("idCategorie");
        $c = ModeleCategorie::selectionner($idCategorie);
        if ($c == false) {
            $vue = 'error';
            $pagetitle = 'Erreur de lecture';
            require File::build_path(array('vue', 'vue.php'));
        }
        else {
            $tab_p = ModeleCategorie::selectionnerToutProduitDeLaCategorie($idCategorie);
            $vue = 'detail';
            $pagetitle = 'Detail de Categorie';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function modifier() {
        if (Session::est_admin()) {
            $idCategorie = myGet("idCategorie");
            $c = ModeleCategorie::selectionner($idCategorie);

            $actionModif = "modifier";
            $vue = 'modifier';
            $pagetitle = 'Mise a jour de la categorie';
            require File::build_path(array('vue', 'vue.php'));
        }
        else {
            $tab_c = ModeleCategorie::selectionnerTout();
            $vue = 'liste';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function modifie() {
        if (Session::est_admin()) {
            $data = array(
                'idCategorie' => myGet('idCategorie'),
                'nomCategorie' => myGet('nomCategorie'),
                'imgCategorie' => myGet('imgCategorie'),
            );
            if (!is_numeric($data['idCategorie']) || empty($data['nomCategorie']) || empty($data['imgCategorie'])) {
                $vue = 'error';
                $pagetitle = 'Erreur champ invalide';
                require File::build_path(array('vue', 'vue.php'));
                return;
            }
            $isGood = ModeleCategorie::modifier($data);

            if ($isGood == false) {
                $idCategorie = $data['idCategorie'];
                $vue = 'error';
                $pagetitle = 'Erreur de mise a jour de Categorie';
                require File::build_path(array('vue', 'vue.php'));
            }
            else {
                $tab_c = ModeleCategorie::selectionnerTout();
                $vue = 'modifie';
                $pagetitle = 'Categorie modifie';
                require File::build_path(array('vue', 'vue.php'));
            }
        }
        else {
            $tab_c = ModeleCategorie::selectionnerTout();
            $vue = 'liste';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function creer() {
        if (Session::est_admin()) {
            $c = new ModeleCategorie();
            $actionModif = "creer";
            $vue = 'modifier';
            $pagetitle = 'Creation de Categorie';
            require File::build_path(array('vue', 'vue.php'));
        }
        else {
            $tab_c = ModeleCategorie::selectionnerTout();
            $vue = 'liste';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function cree() {
        if (Session::est_admin()) {
            $data = array(
                'nomCategorie' => myGet('nomCategorie'),
                'imgCategorie' => myGet('imgCategorie'),
            );
            if (empty(myGet('idCategorie'))) {
                $data['idCategorie'] = ModeleCategorie::selectMaxId()+1;
            }
            else {
                $data['idCategorie'] = myGet('idProduit');
            }
            if (!is_numeric($data['idCategorie']) || empty($data['nomCategorie']) || empty($data['imgCategorie'])) {
                $vue = 'error';
                $pagetitle = 'Erreur champ invalide';
                require File::build_path(array('vue', 'vue.php'));
                return;
            }
            $isGood = ModeleCategorie::creer($data);
            if ($isGood == false) {
                $vue = 'error';
                $pagetitle = 'Erreur de creation de Categorie';
                require File::build_path(array('vue', 'vue.php'));
            }
            else {
                $tab_c = ModeleCategorie::selectionnerTout();
                $vue = 'cree';
                $pagetitle = 'Categorie cree';
                require File::build_path(array('vue', 'vue.php'));
            }
        }
        else {
            $tab_c = ModeleCategorie::selectionnerTout();
            $vue = 'liste';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function supprimmer() {
        if (Session::est_Admin()) {
            $idCategorie = myGet('idCategorie');
            $isGood = ModeleCategorie::supprimmer($idCategorie);
            if ($isGood == false) {
                $vue = 'error';
                $pagetitle = 'Erreur de suppression de Categorie';
                require File::build_path(array('vue', 'vue.php'));
            }
            else {
                $tab_c = ModeleCategorie::selectionnerTout();
                $vue = 'supprimme';
                $pagetitle = 'Categorie supprime';
                require File::build_path(array('vue', 'vue.php'));
            }
        }
        else {
            $tab_c = ModeleCategorie::selectionnerTout();
            $vue = 'liste';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
}
