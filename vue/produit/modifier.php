<div class="card-panel">
<?php
echo "<form method=\"";
if (Conf::getDebug()) {
    echo "get";
}
else {
    echo "post";
}
echo "\" action=\"index.php\">";
?>
  <fieldset>
    
    <?php
    
    if ($actionModif == "modifier") {
        echo '<legend>Mise a jour :</legend>';
        echo '<input type="hidden" value="modifie" name="action"/>';
    }
    else if ($actionModif == "creer") {
        echo '<legend>Création :</legend>';
        echo '<input type="hidden" value="cree" name="action"/>';
    }
    
    ?>
    <?php
    echo '<input type="text" value="'.htmlspecialchars(static::$object).'" placeholder="Ex : Produit" name="controleur" id="control_id" required hidden/>';
    ?>
      <?php
    if ($actionModif == "modifier") {
      echo '<label for="idProduit_id">idProduit</label> :';
      echo '<p>
      <input type="text" value="'.htmlspecialchars($p->get("idProduit")).'" placeholder="Ex : 1" name="idProduit" id="idProduit_id" required readonly/>
    </p>';
    }
    ?>
    <p>
      <label for="libele_id">Libele</label> :
      <?php
      echo '<input type="text" value="'.htmlspecialchars($p->get("libele")).'" placeholder="Ex : Souris sans-fils" name="libele" id="libele_id" required/>';
      ?>
    </p>
    <p>
      <label for="nombre_id">Nombre</label> :
      <?php
      echo '<input type="number" value="'.htmlspecialchars($p->get("nombre")).'" placeholder="Ex : 2" name="nombre" id="nombre_id" required/>';
      ?>
    </p>
    <p>
      <label for="prix_id">Prix</label> :
      <?php
      echo '<input type="number" value="'.htmlspecialchars($p->get("prix")).'" placeholder="Ex : 2" name="prix" id="prix_id" required/>';
      ?>
    </p>
    <p>
      <label for="image_id">imagePrincipale</label> :
      <?php
      echo '<input type="number" value="'.htmlspecialchars($p->get("imagePrincipale")).'" placeholder="Ex : 2" name="imagePrincipale" id="image_id" required/>';
      ?>
    </p>
      <p>

          <label for="categorie_id">Categorie</label> :
          <select multiple="multiple" name="idCategorie[]" id = "categorie_id" class = "browser-default">
              <?php foreach ($tab_c as $c) {
                  echo  '<option  value = "'.htmlspecialchars($c->get("idCategorie")).'">'.htmlspecialchars($c->get("nomCategorie")).'</option>';
              }?>
          </select>
      </p>
    <p>
        <button class="btn waves-effect waves-light" type="submit" value="envoyer">Envoyer</button>
    </p>
  </fieldset> 
</form>
</div>