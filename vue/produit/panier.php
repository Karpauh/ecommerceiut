<?php
echo '<p><b> Prix du panier : '.$_SESSION['prixPanier'].'€.</b></p>'."\n";
?>
<div class="contenu">
<div class="row">
<?php
$DS = DIRECTORY_SEPARATOR;
foreach ($tab_p as $p => $nombre) {
    $p = unserialize($p);
    $imagePath = "vue".$DS."images".$DS."produit".$DS.$p->get('imagePrincipale').'.jpeg';
    echo '<div class=" card-panel center col s12 m6 l3">';
    echo '<p> Produit d\'id'
        . ' <a href="index.php?action=lire&'
            . 'idProduit='.rawurlencode($p->get('idProduit')).'">'
            . ' ' . htmlspecialchars($p->get('idProduit'))
            . '<img src='.$imagePath.' alt="image_produit" height=100>'."\n"
        . '</a>.</p>';
    echo '<p> Quantitée : ' . htmlspecialchars($nombre).'</p>';
    $action = 'suprimmer';
    require File::build_path(array('vue', 'produit', 'formAjoutSuprPanier.php'));
    echo '</div>';
}

?>
</div>
<?php
$test = unserialize($_SESSION['panier']);
if (!empty($test)) {
    echo '<p>';
    if (isset($_SESSION['login'])) {
        echo '<a href="index.php?controleur=produit&action=passerCommande">'
            . '<button class="btn waves-effect waves-light" type="button">'
            . 'Commander'
            . '</button>'
            . '</a>' . "\n";
    } else {
        echo '<a href="index.php?controleur=client&action=connexion">'
            . '<button class="btn waves-effect waves-light" type="button">'
            . 'Veuillez vous connecter afin de passer la commande'
            . '</button>'
            . '</a>' . "\n";
    }
    echo '</p>';
}
?>
</div>
