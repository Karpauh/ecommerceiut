<?php

echo '<p> <h5> Liste de ' . htmlspecialchars($c->get("nomCategorie")) . '</h5> '. '.</p>';

$DS = DIRECTORY_SEPARATOR;
/*$imagePath = "vue".$DS."images".$DS."categorie".$DS.$c->get('imgCategorie').'.jpeg';
echo '<img src='.$imagePath.' alt="image_categorie" height=100>'."\n";*/

if (Session::est_admin()) {
    echo '<p> <h5> Categorie d\'ID ' . htmlspecialchars($c->get("idCategorie")) . '</h5> '. '.</p>';
    echo '<p> <h5> Categorie d\'image ' . htmlspecialchars($c->get("imgCategorie")) . '</h5> '. '.</p>';
    echo '<p>'
            . '<a href="index.php?action=modifier&controleur=categorie&'
            . 'idCategorie='.rawurlencode($c->get("idCategorie")).'" class="btn waves-effect waves-light">'
                . 'Modifier'
            . '</a>';
    echo '<a href="index.php?action=supprimmer&controleur=categorie&'
            . 'idCategorie='.rawurlencode($c->get("idCategorie")).'" class="btn waves-effect waves-light">'
                . 'Supprimer'
            . '</a>'
        . '</p>'."\n";
}

?>
<div class="contenu" >
<div class="row" >
<?php
$DS = DIRECTORY_SEPARATOR;
foreach ($tab_p as $p) {
    $imagePath = "vue".$DS."images".$DS."produit".$DS.$p->get('imagePrincipale').'.jpeg';
    echo '<div class=" card-panel center col s12 m6 l3">
    <p> '
    . ' <a href="index.php?action=lire&'
        . 'idProduit='.rawurlencode($p->get('idProduit')).'">'
        . ' ' . htmlspecialchars($p->get('libele'))
            . '<img src='.$imagePath.' alt="image_produit" height=100>'."\n"
            . '</a>.'
        . '</p> 
    </div>'."\n";
}
?>
</div>
</div>