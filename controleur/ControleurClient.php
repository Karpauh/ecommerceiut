<?php
require_once File::build_path(array('modele', 'ModeleClient.php'));
require_once File::build_path(array('lib', 'Securite.php'));
require_once File::build_path(array('lib', 'Mail.php'));

class ControleurClient {
    
    protected static $object = "client";
    
    public static function lireTout() {
        if(Session::est_admin()) {
            $tab_c = ModeleClient::selectionnerTout();     //appel au modèle pour gerer la BD
            $vue = 'liste';
            $pagetitle = 'Liste des clients';
            require File::build_path(array('vue', 'vue.php'));  //"redirige" vers la vue
        }
        else {
            $vue = 'connexion';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function lire() {
        $login = myGet("login");
        if(Session::est_utilisateur($login)) {  
            $c = ModeleClient::selectionner($login);
            if ($c == false) {
                $vue = 'error';
                $pagetitle = 'Erreur de lecture';
                require File::build_path(array('vue', 'vue.php'));
            }
            else {
                $vue = 'detail';
                $pagetitle = 'Detail de Client';
                $estadmin = ModeleClient::estAdmin($login);
                require File::build_path(array('vue', 'vue.php'));
            }
        }
        else {
            $vue = 'connexion';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function modifier() {
        $login = myGet("login");
        if (Session::est_utilisateur($login)) {
            $c = ModeleClient::selectionner($login);
            $creer = "Modification";
            $actionModif = "modifier";
            $vue = 'modifier';
            $pagetitle = 'Mise a jour du client';
            require File::build_path(array('vue', 'vue.php'));
        }
        else {
            $vue = 'connexion';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function modifie() {
        $data = array(
            'login' => myGet('login'),
            'prenom' => myGet('prenom'),
            'nom' => myGet('nom'),
            'mail' => myGet('mail'),
        );
        if (Session::est_utilisateur($data['login'])) {
            if (strcmp(myGet('passCode'), myGet('passCode2') == 0)) {
                if (Session::est_admin()) {
                    if (!is_null(myGet('admin'))) {
                        if (myGet('admin') == 'on') {
                            $data['admin'] = "1";
                        }
                    }
                    else  {
                        $data['admin'] = "0";
                    }
                }
                $data['passCode'] = Securite::chiffrer(myGet('passCode'));
                $isGood = ModeleClient::modifier($data);
                if ($isGood == false) {
                    $login = $data['login'];
                    $vue = 'error';
                    $pagetitle = 'Erreur de mise a jour de client';
                    require File::build_path(array('vue', 'vue.php'));
                }
                else {
                    $tab_c = ModeleClient::selectionnerTout();
                    $vue = 'modifie';
                    $pagetitle = 'Utilisateur modifiee';
                    require File::build_path(array('vue', 'vue.php'));
                }
            }
            else {
                $login = $data['login'];
                $vue = 'error';
                $pagetitle = 'Les mots de passes sont diferents';
                require File::build_path(array('vue', 'vue.php'));
            }
        }
        else {
            $login = $data['login'];
            $vue = 'connexion';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function creer() {
        $c = new ModeleClient(array ("login" => null, "prenom" => null, "nom" => null, "mail" => null, "passCode" => null));
        $creer = 'Créer un compte' ;
        $actionModif = "creer";
        $vue = 'modifier';
        $pagetitle = 'Creation de Client';
        require File::build_path(array('vue', 'vue.php'));
    }
    
    public static function cree() {

        if(!(preg_match('@[a-z0-9]{3,10}@', myGet('login'))) || false!=ModeleClient::selectionner(myGet('login'))){
            //pseudo saisi n'est pas valide 
            $nom1 = myGet("nom");
            $prenom1 = myGet('prenom');
            $mail1 = myGet("mail");
            
            $creer = 'Créer un compte';
            $actionModif = "creer";
            $vue = 'modifier';
            $pagetitle = 'login non valide';
            require File::build_path(array('vue', 'vue.php'));
        }
        
        elseif(!preg_match('@[a-z]{3,15}@m', myGet('prenom'))){
            //prenom_valide
            
            $nom1 = myGet("nom");
            $login1 = myGet('login');
            $mail1 = myGet("mail");
            
            $creer = 'Créer un compte';
            $actionModif = "creer";
            $vue = 'modifier';
            $pagetitle = 'prenom non valide';
            require File::build_path(array('vue', 'vue.php'));
        }
        
        elseif(!(preg_match('@[a-z]{2,25}@', myGet('nom')))){
            $login1 = myGet("login");
            $prenom1 = myGet('prenom');
            $mail1 = myGet("mail");
            
            $creer = 'Créer un compte';
            $actionModif = "creer";
            $vue = 'modifier';
            $pagetitle = 'nom non valide';
            require File::build_path(array('vue', 'vue.php'));
        }
        elseif(!(preg_match('/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/', myGet("mail")))){
            $nom1 = myGet("nom");
            $prenom1 = myGet('prenom');
            $login1 = myGet("login");
            
            $creer = 'Créer un compte';
            $actionModif = "creer";
            $vue = 'modifier';
            $pagetitle = 'mail non valide';
            require File::build_path(array('vue', 'vue.php'));
            //test email valide 
        }
        else{

        $data = array(
            'login' => myGet('login'),
            'prenom' => myGet('prenom'),
            'nom' => myGet('nom'),
            'mail' => myGet('mail'),
        );
        if (strcmp(myGet('passCode'), myGet('passCode2')) == 0) {
            if (filter_var($data["mail"], FILTER_VALIDATE_EMAIL)!=false) {
                $data['passCode'] = Securite::chiffrer(myGet('passCode'));
                $data['nonce'] = Securite::genererRandomHex();
                $isGood = ModeleClient::creer($data);
                if ($isGood == false) {
                    $vue = 'error';
                    $pagetitle = 'Erreur de creation d\'utilisateur';
                    require File::build_path(array('vue', 'vue.php'));
                }
                else {
                    $mail = Mail::getMail($data['login'], $data['nonce']);
                    mail($data['mail'], "Validation du compte", $mail);
                    $erreur = false;
                    $vue = 'connexion';
                    $pagetitle = 'Compte cree, page de connexion';
                    require File::build_path(array('vue', 'vue.php'));
                }
            }
            else {
                $login = $data['login'];
                $vue = 'error';
                $pagetitle = 'Email non-valide';
                require File::build_path(array('vue', 'vue.php'));
            }
        }
        else {
            $login = $data['login'];
            $vue = 'error';
            $pagetitle = 'Les mots de passes sont diferents';
            require File::build_path(array('vue', 'vue.php'));
        }
        }
    }
    
    public static function supprimmer() {
        $login = myGet('login');
        if (Session::est_utilisateur($login)) {
            $isGood = ModeleClient::supprimmer($login);
            if ($isGood == false) {
                $vue = 'error';
                $pagetitle = 'Erreur de suppression de Client';
                require File::build_path(array('vue', 'vue.php'));
            }
            else {
                if (!Session::est_admin()) {
                    ControleurClient::deconnexion();
                }
                else {
                    $tab_c = ModeleClient::selectionnerTout();
                    $vue = 'supprimme';
                    $pagetitle = 'Client supprime';
                    require File::build_path(array('vue', 'vue.php'));
                }
            }
        }
        else {
            $vue = 'connexion';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function connexion() {
        $erreur = false;
        $vue = 'connexion';
        $pagetitle = 'Page de connexion';
        require File::build_path(array('vue', 'vue.php'));
    }
    
    public static function connecte() {
        $login = myGet('login');
        $mdp = Securite::chiffrer(myGet('passCode'));

        $estValide = ModeleClient::verifierMDP($login, $mdp);
        if ($estValide) {
            $nonceVide = ModeleClient::nonceVide($login);
            if ($nonceVide) {
                $_SESSION['login'] = $login;
                $_SESSION['admin'] = ModeleClient::estAdmin($login);
                $erreur = false;
                $c = ModeleClient::selectionner($login);
                $estadmin = ModeleClient::estAdmin($login);
                $vue = 'detail';
                $pagetitle = 'Connexion reussie';
                require File::build_path(array('vue', 'vue.php'));
            } else {
                $erreur = true;
                $vue = 'connexion';
                $pagetitle = "Veuillez confirmer l'email";
                require File::build_path(array('vue', 'vue.php'));
            }
        }
        else {
            $erreur = true;
            $vue = 'connexion';
            $pagetitle = 'Erreur lors de la connexion';
            require File::build_path(array('vue', 'vue.php'));
        }

    }
    
    public static function deconnexion() {
        if (isset($_SESSION['login'])) {
            session_unset();     // unset $_SESSION variable for the run-time
            setcookie(session_name(),'',time()-1); // deletes the session cookie containing the session ID
            $vue = 'connexion';
            $pagetitle = 'Deconnexion reussie';
            require File::build_path(array('vue', 'vue.php'));
        }
        else {
            $vue = 'connexion';
            $pagetitle = 'Erreur : non connecte';
            require File::build_path(array('vue', 'vue.php'));
        }
    }
    
    public static function valider() {
        $login = myGet('login');
        $nonce = myGet("nonce");
        $nonceOK = ModeleClient::verifierNonce($login, $nonce);
        if ($nonceOK) {
            ModeleClient::modifier(array("login" => $login, "nonce" => NULL));
            $vue = 'connexion';
            $pagetitle = 'Validation reussie';
            require File::build_path(array('vue', 'vue.php'));
        }
        else {
            $vue = 'error';
            $pagetitle = 'Erreur de validation';
            require File::build_path(array('vue', 'vue.php'));
        }
    }

    public static function changeAdmin(){
        $login = myGet('login');
        $c = ModeleClient::selectionner($login);
        if (Session::est_admin()) {
            if ($_SESSION['login']!=$c->get('login')) {
                $estadmin = ModeleClient::estAdmin($login);

                ModeleClient::changeAdmin($login, $estadmin);

                $estadmin = ModeleClient::estAdmin($login);

                $pagetitle = 'Droits modifiés';
                $vue = 'detail';
                $c = ModeleClient::selectionner($login);
                require File::build_path(array('vue', 'vue.php'));
            }
            else {
                $vue = 'detail';
                $pagetitle = 'Erreur : même compte';
                $estadmin = ModeleClient::estAdmin($login);
                require File::build_path(array('vue', 'vue.php'));
            }
        }
        else {
            $vue = 'connexion';
            $pagetitle = 'Connexion requise';
            require File::build_path(array('vue', 'vue.php'));
        }
    }

}