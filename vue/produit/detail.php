<?php


echo '<p><h5> Produit de libele ' . htmlspecialchars($p->get("libele")) . '.</h5></p>'."\n";
echo '<p><h5> Produit de quantité ' . htmlspecialchars($p->get("nombre")) . '.</h5></p>'."\n";
echo '<p><h5> Produit de prix ' . htmlspecialchars($p->get("prix")) . '€.</h5></p>'."\n";


if (Session::est_admin()) {
    echo '<p><h5> Produit d\'id ' . htmlspecialchars($p->get("idProduit")) . '.</h5></p>'."\n";
    echo '<p><h5> Produit d\'imagePrincipale ' . htmlspecialchars($p->get("imagePrincipale")) . '.</h5></p>'."\n";


    echo '<p>'
            . '<a href="index.php?action=modifier&controleur=produit&'
            . 'idProduit='.rawurlencode($p->get("idProduit")).'" class="btn waves-effect waves-light">'
                    . 'Modifier'
                . '</a>'."\n";
    echo '<a href="index.php?action=supprimmer&controleur=produit&'
            . 'idProduit='.rawurlencode($p->get("idProduit")).'" class="btn waves-effect waves-light">'
                    . 'Supprimer'
                . '</a>'."\n";
    echo '</p>';
}


$action = 'ajout';
require File::build_path(array('vue', 'produit', 'formAjoutSuprPanier.php'));

$DS = DIRECTORY_SEPARATOR;
foreach ($imagesPath as $value) {
    $imagePath = "vue".$DS."images".$DS."produit".$DS.$value.'.jpeg';
    echo '<img src='.$imagePath.' alt="image_produit" height=100>'."\n";
}

if(isset($_SESSION["login"])){
     echo '<br><form method="post" action="index.php?action=laisserAvis&controleur=produit&idProduit='. rawurlencode($p->get("idProduit")).'">
      <mat-label><div style="text-align: center; font-weight: bold;">Donnez votre avis sur ce produit</div></mat-label>
      <textarea name="commentaire"></textarea>
    ';

    
    echo '<input type=submit class="btn waves-effect waves-light" value="Laissez un avis" style="margin-left: 45vw;">'
                 . "</input>"
                . '</form>'."\n";
    }
?>


<div class="contenu" >
<div class="row" >

<?php
echo '<p><b>Liste des avis</b></p>';
if(isset($tab_avis)){
foreach ($tab_avis as $a) {
    echo "<p>" . $a ."</p>";
}
}

echo '<p><b>Categories</b></p>';
$DS = DIRECTORY_SEPARATOR;
foreach ($tab_c as $c) {
    $imagePath = "vue".$DS."images".$DS."categorie".$DS.$c->get('imgCategorie').'.jpeg';
    echo '<div class=" card-panel center col s12 m6 l3">
    <p> '
    . ' <a href="index.php?controleur=categorie&action=lire&'
        . 'idCategorie='.rawurlencode($c->get('idCategorie')).'">'
        . ' ' . htmlspecialchars($c->get('nomCategorie'))
            . '<img src='.$imagePath.' alt="image_categorie" height=100>'."\n"
            . '</a>.'
        . '</p> 
    </div>'."\n";
}
?>
</div>
</div>

