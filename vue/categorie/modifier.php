<div class="card-panel">
<?php
echo "<form method=\"";
if (Conf::getDebug()) {
    echo "get";
}
else {
    echo "post";
}
echo "\" action=\"index.php\">";
?>
<?php
$idCategorie = "";
$nomCategorie = "";
$imgCategorie = "";

if(isset($nomCategorie)){
    $nomCategorie = myGet('nomCategorie');
}
if(isset($imgCategorie)){
    $imgCategorie = myget("imgCategorie");
}
if(isset($idCategorie)){
    $idCategorie = myget("idCategorie");
}

?>
  <fieldset>
    <?php
    if ($actionModif == "modifier") {
        echo '<legend>Mise a jour :</legend>';
        echo '<input type="hidden" value="modifie" name="action"/>';
        $nomCategorie = htmlspecialchars($c->get("nomCategorie"));
        $imgCategorie = htmlspecialchars($c->get("imgCategorie"));
        $idCategorie = htmlspecialchars($c->get("idCategorie"));
    }
    else if ($actionModif == "creer") {
        echo '<legend>Création :</legend>';
        echo '<input type="hidden" value="cree" name="action"/>';
    }
    echo '<input type="text" value="'.htmlspecialchars(static::$object).'" placeholder="Ex : Produit" name="controleur" id="control_id" required hidden/>';
    ?>
    <p>
      
      <?php
      if ($actionModif == "modifier") {
        echo '<label for="idCategorie_id">id Categorie</label> :';
        echo '<input type="text" value="'. htmlspecialchars($idCategorie) .'" placeholder="Ex : 0" name="idCategorie" id="idCategorie_id" readonly';
        echo ' />';
      }
      ?>
    </p>
    <p>
      <label for="nomCategorie_id">Nom Categorie</label> :
      <?php
      echo '<input type="text" value="'.htmlspecialchars($nomCategorie).'" placeholder="Ex : Clavier" name="nomCategorie" id="nomCategorie_id" required/>';
      ?>
    </p>
    <p>
      <label for="imgCategorie_id">Image Categorie</label> :
      <?php
      echo '<input type="text" value="'.htmlspecialchars($imgCategorie).'" placeholder="Ex : 3" name="imgCategorie" id="imgCategorie_id" required/>';
      ?>
    </p>
    <p>
        <button class="btn waves-effect waves-light" type="submit" value="envoyer">Envoyer</button>
    </p>
  </fieldset> 
</form>
</div>