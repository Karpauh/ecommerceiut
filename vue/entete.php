<li><a href="index.php?action=lireTout">Accueil</a></li>
<li><a href="index.php?controleur=categorie&action=lireTout">Liste des categories</a></li>
<li><a href="index.php?action=chercherUnProduit">Rechercher un produit</a></li>


<?php
if (Session::est_admin()) {
    echo '<li><a href="index.php?action=creer">Creer un Produit</a></li>';
    echo '<li><a href="index.php?controleur=client&action=lireTout">Liste des clients</a></li>'."\n";
    echo '<li><a href="index.php?controleur=categorie&action=creer">Creer une categorie</a></li>'."\n";
    echo '<li><a href="index.php?controleur=achat&action=creer">Creer un achat</a></li>'."\n";
    echo '<li><a href="index.php?controleur=achat&action=lireTout">Liste achat</a></li>'."\n";
}
        
echo '<li style="float:right"><a href="index.php?action=panier">Panier';
if (isset($_SESSION['prixPanier'])) {
    echo ' : '.$_SESSION['prixPanier'].'€';
}
echo '</a></li>'."\n";

if (isset($_SESSION['login'])) {
    echo '<li style="float:right"><a href="index.php?controleur=client&action=deconnexion">Déconnexion</a></li>'."\n";
    echo '<li style="float:right"><a href="index.php?controleur=client&action=lire&login='.rawurldecode($_SESSION['login']).'">'
            . 'Bonjour, '.htmlspecialchars($_SESSION['login']).'.</a></li>'."\n";
}
else {
    echo '<li style="float:right"><a href="index.php?controleur=client&action=connexion">Connexion</a></li>'."\n";
    echo '<li style="float:right"><a href="index.php?controleur=client&action=creer">Inscription</a></li>'."\n";
}