<?php
require_once File::build_path(array('modele', 'Modele.php'));

class ModeleCategorie extends Modele {
    
    protected static $object = "categorie";
    protected static $primary = 'idCategorie';
        
    private $idCategorie;
    private $nomCategorie;
    private $imgCategorie;


    public function get($nom_attribut) {
        return $this->$nom_attribut;
    }
    
    public function set($nom_attribut, $valeur) {
        $this->$nom_attribut = $valeur;
    }
    
    public function __construct($data = NULL) {
        foreach ((array)$data as $cle => $value) {
            $this->set($cle, $value);
        }
    }
    
    public static function selectionnerToutProduitDelaCategorie($idCategorie) {
        try {
            $rep = Modele::$pdo->prepare("SELECT P.idProduit, P.libele, P.nombre, P.imagePrincipale, P.prix"
                    . " FROM produit P"
                    . " JOIN categorieProduit CP on P.idProduit = CP.idProduit"
                    . " JOIN categorie C on CP.idCategorie = C.idCategorie"
                    . " WHERE CP.idCategorie = :ic");
            
            $values = array(
                "ic" => $idCategorie,
            );
            
            $rep->execute($values);

            $rep->setFetchMode(PDO::FETCH_CLASS, 'ModeleCategorie');
            return (array)$rep->fetchAll();
        }
        catch(PDOException $e) {
            if (Conf::getDebug()) {
                echo $e->getMessage(); // affiche un message d'erreur
            } else {
                echo 'Une erreur est survenue <a href="index.php?action=lireTout"> retour a la page d\'accueil </a>';
            }
            die();
        }
    }
    public static function associerProduitCategorie($data){
        $table_name = 'categorieProduit';
        try {
            $sql = "INSERT INTO $table_name (";
            foreach ($data as $name => $value) {
                $sql = $sql."$name";
                if ($name != array_key_last($data)) {
                    $sql = $sql.", ";
                }
            }
            $sql = $sql. ") VALUES (";
            foreach ($data as $name => $value) {
                $sql = $sql.":$name";
                if ($name != array_key_last($data)) {
                    $sql = $sql.", ";
                }
            }
            $sql = $sql.")";
            $rep_prep = Modele::$pdo->prepare($sql);

            $rep_prep->execute($data);
            return true;
        }
        catch(PDOException $e) {
            if (Conf::getDebug()) {
                echo $e->getMessage(); // affiche un message d'erreur
            }
            return false;
        }
    }

    public static function modifProduitCategorie($data){
        $table_name = 'categorieProduit';
        $primary_key = 'idProduit';
        $primary_key0 = 'idCategorie';
        try {
            $sql = "UPDATE $table_name "
                . "SET ";

            foreach ($data as $name => $value) {
                $sql = $sql."$name = :$name";
                if ($name != array_key_last($data)) {
                    $sql = $sql.", ";
                }
            }

            $sql = $sql." WHERE $table_name.$primary_key = :$primary_key AND $table_name.$primary_key0";
            $rep_prep = Modele::$pdo->prepare($sql);

            $rep_prep->execute($data);
            return true;
        } catch (PDOException $e) {
            if (Conf::getDebug()) {
                echo $e->getMessage(); // affiche un message d'erreur
            }
            return false;
        }
    }
    public static function supprimerToutcateid($idProd){
        $table_name = 'categorieProduit';
        $primary_key = 'idProduit';
        $primary_key0 = 'idCategorie';
        try {
            $sql = "DELETE FROM $table_name "
                . "WHERE $primary_key = :idProd ";

            $values =  array (
                "idProd" => $idProd,
            );
            $rep_prep = Modele::$pdo->prepare($sql);

            $rep_prep->execute($values);
            return true;
        } catch (PDOException $e) {
            if (Conf::getDebug()) {
                echo $e->getMessage(); // affiche un message d'erreur
            }
            return false;
        }
    }
    
    public static function selectMaxId() {
        try {
            $rep = Modele::$pdo->query("SELECT MAX(C.idCategorie)"
                    . " FROM categorie C");


            $rep->setFetchMode(PDO::FETCH_COLUMN, 0);
            return $rep->fetchAll()[0];
        }
        catch(PDOException $e) {
            if (Conf::getDebug()) {
                echo $e->getMessage(); // affiche un message d'erreur
            } else {
                echo 'Une erreur est survenue <a href="index.php?action=lireTout"> retour a la page d\'accueil </a>';
            }
            die();
        }
    }

}