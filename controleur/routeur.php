<?php
require_once File::build_path(array('controleur','ControleurProduit.php'));
require_once File::build_path(array('controleur','ControleurClient.php'));
require_once File::build_path(array('controleur','ControleurCategorie.php'));
require_once File::build_path(array('controleur','ControleurAchat.php'));

function myGet($nomVar) {
    if (isset($_POST[$nomVar])){
        return $_POST[$nomVar];
    }
    else if (isset($_GET[$nomVar])) {
        return $_GET[$nomVar];
    }
    else {
        return NULL;
    }
}

function myCookie($nomVar) {
    if (isset($_COOKIE[$nomVar])) {
        return $_COOKIE[$nomVar];
    }
    else {
        return null;
    }
}

// On recupère l'action passée dans l'URL
if (!is_null(myget("action"))){
    $action = myget("action");
}
else {
    $action = "lireTout" ;   
}

// On recupère le controller passée dans l'URL
if (!is_null(myget("controleur"))) {
    $controller = myget("controleur");
}
else {
    $controller = "produit";
}

$controller_class = "Controleur".ucfirst($controller);

if (class_exists($controller_class)) {
    if (in_array($action, get_class_methods($controller_class))) {
        $controller_class::$action();
    }
    else {
        $vue = 'error';
        $pagetitle = 'Erreur de methode';
        require File::build_path(array('vue', 'vue.php'));
    }
}
else {
    $vue = 'error';
    $pagetitle = 'Erreur, controleur non valide';
    require File::build_path(array('vue', 'vue.php'));
}
    

?>
