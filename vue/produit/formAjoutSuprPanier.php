<?php
echo "<form method=\"";
if (Conf::getDebug()) {
    echo "get";
}
else {
    echo "post";
}
echo "\" action=\"index.php\">"."\n";
?>
    <fieldset>
        <legend>
            <?php
            if ($action == 'ajout') {
                echo 'Ajouter au panier :</legend>';
                echo '<input type="hidden" value="ajouterPanier" name="action" hidden/>';
                echo '<input type="text" value="produit" placeholder="Ex : Produit" name="controleur" id="control_id" hidden/>';
            }
            else if ($action == 'suprimmer') {
                echo 'Supprimer du panier :</legend>';
                echo '<input type="hidden" value="supprimerPanier" name="action" hidden/>';
            }

            
            echo '<input type="hidden" value="'.htmlspecialchars($p->get('idProduit')).'" name="idProduit" hidden/>';
        ?>
        <p>
            <label>Quantité : <input type="number" value="1" placeholder="Ex : 1" name="quantite" required/></label>
            
        </p>
        <?php
        if ($action == 'ajout') {
            echo '<button class="btn waves-effect waves-light" type="submit" value="envoyer">Ajouter au Panier</button>';
        }
        else {
            echo '<button class="btn waves-effect waves-light" type="submit" value="envoyer">Supprimer du Panier</button>';
        }
        ?>
    </fieldset> 
</form>