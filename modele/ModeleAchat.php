<?php
require_once File::build_path(array('modele', 'Modele.php'));

class ModeleAchat extends Modele {
    
    protected static $object = "achat";
    protected static $primary = 'idAchat';
        
    private $idAchat;
    private $login;
    private $idProduit;
    private $date;
    private $nombre;

    public function get($nom_attribut) {
        return $this->$nom_attribut;
    }
    
    public function set($nom_attribut, $valeur) {
        $this->$nom_attribut = $valeur;
    }
    
    public function __construct($data = NULL) {
        foreach ((array)$data as $cle => $value) {
            $this->set($cle, $value);
        }
    }
}