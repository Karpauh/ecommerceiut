<div class="card-panel">
<?php
echo "<form method=\"";
if (Conf::getDebug()) {
    echo "get";
}
else {
    echo "post";
}
echo "\" action=\"index.php\">";
?>
<fieldset>
    <?php
    if ($actionModif == "modifier") {
        echo '<legend>Mise a jour :</legend>';
        echo '<input type="hidden" value="modifie" name="action"/>';
    }
    else if ($actionModif == "cree") {
        echo '<legend>Création :</legend>';
        echo '<input type="hidden" value="cree" name="action"/>';
    }
    echo '<input type="text" value="'.htmlspecialchars(static::$object).'" placeholder="Ex : Produit" name="controleur" id="control_id" required hidden/>';

      echo '<input type="hidden" value="'. htmlspecialchars($a->get('idAchat')) .'" placeholder="Ex : 0" name="idAchat" hidden/>';
      ?>
    </p>
    <p>
      <label for="login_id">Login</label> :
      <?php
      echo '<input type="text" value="'.htmlspecialchars($a->get('login')).'" placeholder="Ex : Clavier" name="login" id="login_id" required/>';
      ?>
    </p>
    <p>
      <label for="idProduit_id">id Produit</label> :
      <?php
      echo '<input type="text" value="'.htmlspecialchars($a->get('idProduit')).'" placeholder="Ex : 3" name="idProduit" id="idProduit_id" required/>';
      ?>
    </p>
    <p>
      <label for="nombre_id">nombre</label> :
      <?php
      echo '<input type="text" value="'.htmlspecialchars($a->get('nombre')).'" placeholder="Ex : 3" name="nombre" id="nombre_id" required/>';
      ?>
    </p>
    <?php
    if ($actionModif == "modifier") {
        echo '<p>
        <label for="date_id">date</label> :
        <input type="date" value="'.htmlspecialchars($a->get('date')).'" placeholder="Ex : 2019-10-18 00:00:00" name="date" id="date_id" required/>';
        echo '</p>';
    }
    ?>
    <p>
        <button class="btn waves-effect waves-light" type="submit" value="envoyer">Envoyer</button>
    </p>
  </fieldset> 
</form>
</div>