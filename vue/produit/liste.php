<div class="contenu" >
<div class="row" >
<?php
$DS = DIRECTORY_SEPARATOR;
foreach ($tab_p as $p) {
    $imagePath = "vue".$DS."images".$DS."produit".$DS.$p->get('imagePrincipale').'.jpeg';
    echo '<div class=" card-panel center col s12 m6 l3">
    <p> '
    . ' <a href="index.php?action=lire&'
        . 'idProduit='.rawurlencode($p->get('idProduit')).'">'
        . ' ' . htmlspecialchars($p->get('libele'))
            . '<img src='.$imagePath.' alt="image_produit" height=100>'."\n"
            . '</a>.'
        . '</p> 
    </div>'."\n";
}
?>
</div>
</div>
