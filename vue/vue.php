<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $pagetitle; ?></title>
        <!-- Materialize: Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link rel="stylesheet" type="text/css" href="vue/css/styles.css">
        <!-- Google Roboto -->
        <link rel="stylesheet" href='http://fonts.googleapis.com/css?family=Roboto'>
    </head>
    
    <body class="amber lighten-5">
        
        <header>
            <nav>
                <ul>
                    <?php
                    require File::build_path(array("vue", "entete.php"));
                    ?>
                </ul>
            </nav>
        </header>
    
    
        
        <?php
            echo '<p> <h1 class="center ">  '.$pagetitle.'  </h1>  </p>';
        
            $filepath = File::build_path(array("vue", static::$object, "$vue.php"));
            require $filepath;
        ?>
        
        
        <p class="footer">
            Site de vente stylé
        </p>
    </body>
</html>
