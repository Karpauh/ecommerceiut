<div class="card-panel">
<?php
echo "<form method=\"";
if (Conf::getDebug()) {
    echo "get";
}
else {
    echo "post";
}
echo "\" action=\"index.php\">"."\n";
?>
    <fieldset>
        <legend>Rechercher un produit</legend>
        <input type="hidden" name="action" value="chercher" hidden/>
        <input type="hidden" name="controller" value="produit" hidden/>

        <p>
            <label for="libele_id">Nom du produit</label> :
            <input type="text" placeholder="Processeur intel i9-5540" id="libele_id" name="libele"/>
        </p>
        <p>
            <label for="prix_id">Prix du produit</label> :
            <input type="number" placeholder="60" id="prix_id" name="prix"/>
        </p>
       
        
        <button class="btn waves-effect waves-light" type="submit" value="envoyer">Rechercher</button>
    </fieldset>
</form>
</div>